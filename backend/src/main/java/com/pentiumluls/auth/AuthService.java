package com.pentiumluls.auth;

import com.pentiumluls.player.Player;
import com.pentiumluls.player.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService {
    private final PlayerRepository playerRepository;

    @Autowired
    public AuthService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public Optional<Player> login(AuthRequest request) {
        return playerRepository.findByName(request.getName());
    }
}
