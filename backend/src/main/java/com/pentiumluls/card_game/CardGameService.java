package com.pentiumluls.card_game;

import com.pentiumluls.card_game.model.GameCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardGameService {
    private GameCardRepository gameCardRepository;

    @Autowired
    public CardGameService(GameCardRepository gameCardRepository) {
        this.gameCardRepository = gameCardRepository;
    }

    public List<GameCard> getPlayerCards(Long playerId) {
        return gameCardRepository.findAllByPlayerId(playerId);
    }
}
