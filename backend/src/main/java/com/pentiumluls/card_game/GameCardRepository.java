package com.pentiumluls.card_game;

import com.pentiumluls.card_game.model.GameCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameCardRepository extends JpaRepository<GameCard, Long> {
    List<GameCard> findAllByPlayerId(Long playerId);
}
