package com.pentiumluls.card_game.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pentiumluls.player.Player;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "game_cards")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class GameCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private GameCardType type;

    @Column(nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(name = "`rank`", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private GameCardRank rank;

    @Column(nullable = false)
    private int health;

    @Column(nullable = false)
    private int damage;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "player_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    Player player;
}
