package com.pentiumluls.card_game.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum GameCardType {
    HARPY(0),
    ;

    public final int type;
}
