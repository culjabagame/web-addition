package com.pentiumluls.card_game.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum GameCardRank {
    FIRST(1),
    SECOND(2),
    THIRD(3),
    ;

    public final int rank;
}
