package com.pentiumluls.card_game;

import com.pentiumluls.card_game.model.GameCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/** Be aware! This package probably will be as separate microservice.
 * As in plans, transport protocol will be UDP, so users can play with other players in realtime */
@RestController
@RequestMapping("/api/card_game")
@CrossOrigin("*")
public class CardGameController {
    private CardGameService cardGameService;

    @Autowired
    public CardGameController(CardGameService cardGameService) {
        this.cardGameService = cardGameService;
    }

    @GetMapping("/cards")
    public List<GameCard> getPlayerCards(@RequestParam Long playerId) {
        return cardGameService.getPlayerCards(playerId);
    }
}
