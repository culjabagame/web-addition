# Storage
Search `Ngrx` and `ngrx-store-localstorage` for more details

There are one `root` store and many `feature` storages.
For example, on picture below there is an `appState` (which is a root) and `cardGame`(feature)

![](/home/jojo/IdeaProjects/Java2DGame/web-addition/frontend/doc/storage_example.png)

## Root Store
at `src/app/state`

## Feature Stores
at `src/app/<FEATURE>/state`
