# Frontend of WEB-based addition to the game

## Run
To start, just run 
```bash
ng serve
```

## Deploy / run locally in docker
See instructions at the top level repository (https://gitlab.com/culjabagame/2d-java-game)

## Guides
 - [Storage](./doc/Storage.md) - how to add feature ngrx storage and preserve it's state after page reload

## Dependencies:
 - [ngrx-store-localstorage](https://github.com/btroncone/ngrx-store-localstorage)
For syncing ngrx state with localstorage.
