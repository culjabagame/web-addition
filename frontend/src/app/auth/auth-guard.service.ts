import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {AuthService} from "./auth.service";
import {RouterService} from "../shared/services/router.service";

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private auth: AuthService,
              private routerService: RouterService) {
  }

  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.routerService.navigateLogin();
      return false;
    }
    return true;
  }
}
