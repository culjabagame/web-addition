import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {Player} from "../shared/models/player.model";
import {environment} from "../../environments/environment";
import {Store} from "@ngrx/store";
import {selectPlayer} from "../state/application.selectors";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private baseUrl = `${environment.backendURL}/login`;
  private player!: Player;

  constructor(private httpClient: HttpClient,
              private store: Store) {
    store.select(selectPlayer).subscribe(player => this.player = player);
  }

  login(name: string): Observable<Player> {
    return this.httpClient.post<Player>(this.baseUrl, {name});
  }

  public isAuthenticated(): boolean {
    return this.player != undefined;
  }
}
