import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {AuthGuardService} from "./auth-guard.service";
import {FooterComponent} from "../standalone/components/footer/footer.component";

@NgModule({
  providers: [
    AuthGuardService
  ],
  declarations: [
    LoginComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        SharedModule,
        FooterComponent,
    ]
})
export class AuthModule { }
