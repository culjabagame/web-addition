import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../auth.service";
import {RouterService} from "../../shared/services/router.service";
import {Player} from "../../shared/models/player.model";
import {Store} from "@ngrx/store";
import {ApplicationActions} from "../../state/application.actions";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(private authService: AuthService,
              private routerService: RouterService,
              private store: Store) {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    let val = this.form.value;
    if (!val.name)
      return;

    this.authService.login(val.name)
      .subscribe({
        next: (player) => this.onLoginSuccess(player),
        error: (error) => this.onLoginError(error),
      });
  }

  private onLoginSuccess(player: Player) {
    this.store.dispatch(ApplicationActions.logIn(player));
    this.routerService.navigateHome();
  }

  private onLoginError(error: any) {
    if (error.status === 401) {
      this.form.controls['name'].setErrors({ invalid: true });
    }
  }
}
