import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlayersListComponent} from './players-list/players-list.component';
import {SharedModule} from "../shared/shared.module";
import {HeaderComponent} from "../standalone/components/header/header.component";
import {FooterComponent} from "../standalone/components/footer/footer.component";
import {TriStateSwitcherComponent} from "../standalone/components/tri-state-switcher/tri-state-switcher.component";
import {FieldFilterPipe} from "../standalone/pipes/field-filter.pipe";

@NgModule({
  declarations: [
    PlayersListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HeaderComponent,
    FooterComponent,
    TriStateSwitcherComponent,
  ],
  providers: [
    FieldFilterPipe,
  ]
})
export class PlayersModule { }
