import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Player} from "../shared/models/player.model";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {FieldFilterPipe} from "../standalone/pipes/field-filter.pipe";

@Injectable({
  providedIn: 'root'
})
export class PlayersService {
  private baseUrl = `${environment.backendURL}/players`;

  constructor(private httpClient: HttpClient,
              private fieldFilterPipe: FieldFilterPipe) {
  }

  getAllPlayers(): Observable<Player[]> {
    return this.httpClient.get<Player[]>(this.baseUrl);
  }

  /** Returns new list of players, filtered by specified in `playerFilter` fields.
   * Uses `FieldFilterPipe` to filter data */
  filter(players: Player[], playerFilter: any) {
    return this.fieldFilterPipe.transform(players, playerFilter);
  }
}
