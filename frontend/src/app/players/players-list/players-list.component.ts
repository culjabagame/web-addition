import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Player} from "../../shared/models/player.model";
import {PlayersService} from "../players.service";

@Component({
  selector: 'app-players-list',
  templateUrl: './players-list.component.html',
  styleUrls: ['./players-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlayersListComponent implements OnInit {
  players?: Player[];
  playerFilter: any = {online: true} // show both offline and online players
  playerStatusFilter!: 'left' | 'right' | 'both';

  constructor(private playersService: PlayersService,
              private changeDetectorRef: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.playerStatusFilter = 'both'
    this.playersService.getAllPlayers().subscribe(players => {
      this.players = players
      this.updateFilter(this.playerStatusFilter);
      this.changeDetectorRef.detectChanges();
    });
  }

  getPlayersToDisplay(): Player[] {
    if (!this.players)
      return []

    return this.playersService.filter(this.players, this.playerFilter);
  }

  updateFilter(playerStatusFilter: "left" | "right" | "both") {
    this.playerStatusFilter = playerStatusFilter;

    if (this.playerStatusFilter === 'left') {
      this.playerFilter = {...this.playerFilter, online: true};
    } else if (this.playerStatusFilter === 'right') {
      this.playerFilter = {...this.playerFilter, online: false};
    } else {
      // remove online field
      const {online, ...result} = this.playerFilter;
      this.playerFilter = result;
    }
  }
}
