import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {Player} from "../../../shared/models/player.model";
import {selectPlayer} from "../../../state/application.selectors";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";

@Component({
  standalone: true,
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  imports: [CommonModule, RouterModule],
})
export class HeaderComponent implements OnInit {
  player?: Player;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.select(selectPlayer)
      .subscribe((player) => this.player = player)
  }

}
