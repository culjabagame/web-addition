import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CommonModule} from "@angular/common";

/** Switcher with 3 states: left, right or both */
@Component({
  standalone: true,
  selector: 'tri-state-switcher',
  templateUrl: './tri-state-switcher.component.html',
  styleUrls: ['./tri-state-switcher.component.scss'],
  imports: [CommonModule],
})
export class TriStateSwitcherComponent implements OnInit {
  @Input() title1?: string;
  @Input() title2?: string;

  @Input() state!: 'left' | 'right' | 'both';
  @Output() stateChanged = new EventEmitter<'left' | 'right' | 'both'>();

  constructor() {
  }

  ngOnInit(): void {
  }

  leftClicked() {
    this.state = 'left'
    this.stateChanged.emit(this.state);
  }

  rightClicked() {
    this.state = 'right'
    this.stateChanged.emit(this.state);
  }

  middleClicked() {
    this.state = 'both';
    this.stateChanged.emit(this.state);
  }
}
