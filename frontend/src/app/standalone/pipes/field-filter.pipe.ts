import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'fieldFilter',
  standalone: true,
})
export class FieldFilterPipe implements PipeTransform {

  transform(items: any[], filter: any): any {
    if (!items || !filter)
      return items;

    return items.filter(item => this.filterPlayer(item, filter));
  }

  private filterPlayer(item: any, filter: any) {
    for (let field in filter) {
      if (item[field] != filter[field]) {
        return false;
      }
    }
    return true;
  }
}
