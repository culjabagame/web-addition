import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CardGameComponent} from './card-game/card-game.component';
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {cardGameFeature} from "./state/cardGame.reducer";
import {StartScreenComponent} from './start-screen/start-screen.component';
import {CardGameEffects} from "./state/cardGame.effects";
import {CardsOnHandComponent} from './cards-on-hand/cards-on-hand.component';
import {CardComponent} from './card/card.component';
import {CardsRowComponent} from './cards-row/cards-row.component';
import {SharedModule} from "../shared/shared.module";
import {FooterComponent} from "../standalone/components/footer/footer.component";
import {HeaderComponent} from "../standalone/components/header/header.component";

@NgModule({
  declarations: [
    CardGameComponent,
    StartScreenComponent,
    CardsOnHandComponent,
    CardComponent,
    CardsRowComponent,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(cardGameFeature),
    EffectsModule.forFeature([CardGameEffects]),
    SharedModule,
    FooterComponent,
    HeaderComponent,
  ]
})
export class CardGameModule {
}
