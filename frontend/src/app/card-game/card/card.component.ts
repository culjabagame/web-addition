import {Component, Input, OnInit} from '@angular/core';
import {GamingCard} from "../model/gamingCard.model";

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() card!: GamingCard;
  @Input() imageName!: string;
  @Input() isSelected!: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
