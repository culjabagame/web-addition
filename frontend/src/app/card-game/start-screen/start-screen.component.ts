import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {CardGameActions} from "../state/cardGame.actions";

@Component({
  selector: 'app-start-screen',
  templateUrl: './start-screen.component.html',
  styleUrls: ['./start-screen.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StartScreenComponent implements OnInit {

  constructor(private store: Store) { }

  ngOnInit(): void {
  }

  startGame() {
    this.store.dispatch(CardGameActions.start())
  }

}
