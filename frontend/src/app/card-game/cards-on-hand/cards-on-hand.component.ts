import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {GamingCard} from "../model/gamingCard.model";
import {CardGameService} from "../card-game.service";
import {Store} from "@ngrx/store";
import {CardGameActions} from "../state/cardGame.actions";
import {selectSelectedCard} from "../state/cardGame.reducer";

@Component({
  selector: 'cards-on-hand',
  templateUrl: './cards-on-hand.component.html',
  styleUrls: ['./cards-on-hand.component.scss']
})
export class CardsOnHandComponent implements OnInit {
  @Input() cards$!: Observable<GamingCard[]>;
  @Input() playerCanInteract!: boolean;
  selectedCard?: GamingCard | undefined;

  constructor(private cardGameService: CardGameService,
              private store: Store) {
    this.store.select(selectSelectedCard).subscribe(card => this.selectedCard = card);
  }

  ngOnInit(): void {
  }

  getCardImageName(card: GamingCard) {
    return this.cardGameService.getCardImageName(card);
  }

  onCardClicked(card: GamingCard) {
    if (this.playerCanInteract)
      this.store.dispatch(CardGameActions.selectCard({card: card}));
  }

  isCardSelected(card: GamingCard) {
    return this.selectedCard?.id == card.id;
  }
}
