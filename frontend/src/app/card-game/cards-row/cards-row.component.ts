import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {GamingCard} from "../model/gamingCard.model";
import {CardGameService} from "../card-game.service";
import {CardGameActions} from "../state/cardGame.actions";
import {Store} from "@ngrx/store";

@Component({
  selector: 'cards-row',
  templateUrl: './cards-row.component.html',
  styleUrls: ['./cards-row.component.scss']
})
export class CardsRowComponent implements OnInit {
  @Input() cards$!: Observable<GamingCard[]>;
  @Input() isHoverEnabled: boolean = false;
  @Input() rowId!: 'melee' | 'range';

  constructor(private cardGameService: CardGameService,
              private store: Store) {
  }

  ngOnInit(): void {
  }

  getCardImageName(card: GamingCard) {
    return this.cardGameService.getCardImageName(card);
  }

  onRowClicked() {
    if (this.isHoverEnabled)
      this.store.dispatch(CardGameActions.placeCardOnBoard({row: this.rowId}));
  }
}
