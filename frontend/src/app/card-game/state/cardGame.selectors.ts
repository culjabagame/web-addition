import {createSelector} from "@ngrx/store";
import {cardGameFeature} from "./cardGame.reducer";

export const selectPlayerCards = createSelector(
  cardGameFeature.selectPlayer,
  (player) => {return player.cards}
);

export const selectPlayerCardsMelee = createSelector(
  cardGameFeature.selectPlayer,
  (player) => {return player.cardsMelee}
);

export const selectPlayerCardsRange = createSelector(
  cardGameFeature.selectPlayer,
  (player) => {return player.cardsRange}
);

export const selectEnemyCards = createSelector(
  cardGameFeature.selectEnemy,
  (enemy) => {return enemy.cards}
);

export const selectEnemyCardsMelee = createSelector(
  cardGameFeature.selectEnemy,
  (enemy) => {return enemy.cardsMelee}
);

export const selectEnemyCardsRange = createSelector(
  cardGameFeature.selectEnemy,
  (enemy) => {return enemy.cardsRange}
);

