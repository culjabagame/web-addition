import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {RouterService} from "../../shared/services/router.service";
import {CardGameActions} from "./cardGame.actions";
import {catchError, map, of, switchMap} from "rxjs";
import {CardGameService} from "../card-game.service";

@Injectable()
export class CardGameEffects {
  constructor(private actions$: Actions,
              private routerService: RouterService,
              private cardGameService: CardGameService) {
  }

  startGame$ = createEffect(() => this.actions$.pipe(
    ofType(CardGameActions.start),
    switchMap(() => this.routerService.startCardGame()
      .then((isRedirected) => CardGameActions.loadCards()))
  ))

  loadCards$ = createEffect(() => this.actions$.pipe(
    ofType(CardGameActions.loadCards),
    switchMap(() => this.cardGameService.getCards().pipe(
      map(cards => CardGameActions.loadCardsSuccess({cards: cards})),
      catchError(() => of(CardGameActions.loadCardsError))
    ))
  ))
}
