import {GamingCard} from "../model/gamingCard.model";
import {GamingCardType} from "../model/GamingCardType.model";
import {GamingCardRank} from "../model/GamingCardRank.model";

export class CardGameState {
  isStarted: boolean;
  selectedCard: GamingCard | undefined;
  player: Cards;
  enemy: Cards;

  constructor(isStarted: boolean, selectedCard: GamingCard, player: Cards, enemy: Cards,) {
    this.isStarted = isStarted;
    this.selectedCard = selectedCard;
    this.player = player;
    this.enemy = enemy;
  }
}

interface Cards {
  cards: GamingCard[];
  cardsMelee: GamingCard[];
  cardsRange: GamingCard[];
}

export const initialCardGameState: CardGameState = {
  isStarted: false,
  selectedCard: undefined,
  player: {
    cards: [],
    cardsMelee: [],
    cardsRange: [],
  },
  enemy: {
    cards: [
      {id: 100, type: GamingCardType.HARPY, name: 'harpy', health: 5, damage: 5, description: 'description', rank: GamingCardRank.FIRST},
      {id: 101, type: GamingCardType.HARPY, name: 'harpy', health: 5, damage: 5, description: 'description', rank: GamingCardRank.FIRST},
      {id: 102, type: GamingCardType.HARPY, name: 'harpy', health: 5, damage: 5, description: 'description', rank: GamingCardRank.FIRST},
      {id: 103, type: GamingCardType.HARPY, name: 'harpy', health: 5, damage: 5, description: 'description', rank: GamingCardRank.FIRST},
      {id: 104, type: GamingCardType.HARPY, name: 'harpy', health: 5, damage: 5, description: 'description', rank: GamingCardRank.FIRST},
    ],
    cardsMelee: [],
    cardsRange: [],
  }
}
