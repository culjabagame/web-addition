import {createFeature, createReducer, on} from "@ngrx/store";
import {initialCardGameState} from "./cardGame.state";
import {CardGameActions} from "./cardGame.actions";

export const cardGameFeature = createFeature({
  name: 'cardGame',
  reducer: createReducer(
    initialCardGameState,
    on(CardGameActions.start, (state) => {
      return {...state, isStarted: true}
    }),
    on(CardGameActions.stop, (state) => {
      return {...state, isStarted: false}
    }),
    on(CardGameActions.loadCardsSuccess, (state, props) => {
      return {...state, player: {...state.player, cards: props.cards}}
    }),
    on(CardGameActions.selectCard, (state, props) => {
      return {...state, selectedCard: props.card}
    }),
    on(CardGameActions.placeCardOnBoard, (state, props) => {
      // reset selectedCard, remove selected card from hand, adds it to melee or range row
      let newState = {...state, selectedCard: undefined, player: {
          ...state.player,
          cards: state.player.cards.filter(card => card.id !== state.selectedCard?.id)
        }};
      if (props.row === 'melee')
        newState.player.cardsMelee = [...state.player.cardsMelee, state.selectedCard!]
      if (props.row === 'range')
        newState.player.cardsRange = [...state.player.cardsRange, state.selectedCard!]
      return newState
    })
  )
});

export const {
  name,
  reducer,
  selectIsStarted,
  selectPlayer,
  selectEnemy,
  selectSelectedCard,
} = cardGameFeature
