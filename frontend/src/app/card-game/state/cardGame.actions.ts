import {createActionGroup, emptyProps, props} from "@ngrx/store";
import {GamingCard} from "../model/gamingCard.model";

export const CardGameActions = createActionGroup({
  source: 'CARD GAME',
  events: {
    'Start': emptyProps(),
    'Stop': emptyProps(),
    'Load cards': emptyProps(),
    'Load cards success': props<{cards: GamingCard[]}>(),
    'Load cards error': emptyProps(),

    'Select card': props<{card: GamingCard}>(),
    'Place card on board': props<{row: 'melee' | 'range'}>(),
  }
  });
