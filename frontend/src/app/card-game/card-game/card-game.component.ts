import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {selectIsStarted, selectSelectedCard} from "../state/cardGame.reducer";
import {
  selectEnemyCards,
  selectEnemyCardsMelee,
  selectEnemyCardsRange,
  selectPlayerCards,
  selectPlayerCardsMelee,
  selectPlayerCardsRange
} from "../state/cardGame.selectors";
import {Observable} from "rxjs";
import {GamingCard} from "../model/gamingCard.model";
import {CardGameService} from "../card-game.service";

@Component({
  selector: 'card-game',
  templateUrl: './card-game.component.html',
  styleUrls: ['./card-game.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class CardGameComponent implements OnInit {
  isStarted$: Observable<boolean>;
  playerCards$: Observable<GamingCard[]>;
  playerCardsRowMelee$: Observable<GamingCard[]>; // closest to enemy row of player's card
  playerCardsRowRange$: Observable<GamingCard[]>;

  isHoverEnabled: boolean = false;

  enemyCards$: Observable<GamingCard[]>;
  enemyCardsRowMelee$: Observable<GamingCard[]>;
  enemyCardsRowRange$: Observable<GamingCard[]>;

  constructor(private store: Store,
              private cardGameService: CardGameService) {
    this.isStarted$ = this.store.select(selectIsStarted)
    this.playerCards$ = this.store.select(selectPlayerCards)
    this.playerCardsRowMelee$ = this.store.select(selectPlayerCardsMelee)
    this.playerCardsRowRange$ = this.store.select(selectPlayerCardsRange)

    this.enemyCards$ = this.store.select(selectEnemyCards)
    this.enemyCardsRowMelee$ = this.store.select(selectEnemyCardsMelee)
    this.enemyCardsRowRange$ = this.store.select(selectEnemyCardsRange)

    this.store.select(selectSelectedCard)
      .subscribe(card => card
        ? this.isHoverEnabled = true
        : this.isHoverEnabled = false);
  }

  ngOnInit(): void {
  }
}
