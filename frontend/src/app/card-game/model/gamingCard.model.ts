import {GamingCardType} from "./GamingCardType.model";
import {GamingCardRank} from "./GamingCardRank.model";

export class GamingCard {
  id: number;
  type: GamingCardType;
  name: string;
  description: string;
  rank: GamingCardRank;
  health: number;
  damage: number;

  constructor(id: number, type: GamingCardType, name: string, description: string, rank: GamingCardRank, health: number, damage: number) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.description = description;
    this.rank = rank;
    this.health = health;
    this.damage = damage;
  }
}
