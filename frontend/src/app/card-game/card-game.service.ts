import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {GamingCard} from "./model/gamingCard.model";
import {environment} from "../../environments/environment";
import {GamingCardType} from "./model/GamingCardType.model";
import {Store} from "@ngrx/store";
import {selectPlayer} from "../state/application.selectors";

@Injectable({
  providedIn: 'root'
})
export class CardGameService {
  private baseUrl = `${environment.backendURL}/card_game`;
  private playerId?: number;

  constructor(private httpClient: HttpClient,
              private store: Store) {
    this.store.select(selectPlayer).subscribe((player) => this.playerId = player?.id)
  }

  getCards(): Observable<GamingCard[]> {
    return this.httpClient.get<GamingCard[]>(`${this.baseUrl}/cards?playerId=${this.playerId}`);
  }

  getCardImageName(card: GamingCard): string {
    switch (card.type) {
      case GamingCardType.HARPY: return 'harpy.jpg';
    }
  }
}
