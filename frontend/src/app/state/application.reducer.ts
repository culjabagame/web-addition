import {ApplicationState, initialAppState} from "./application.state";
import {ActionReducer, createReducer, MetaReducer, on} from "@ngrx/store";
import {ApplicationActions} from "./application.actions";
import {localStorageSync} from "ngrx-store-localstorage";

export const appReducer = createReducer(
  initialAppState,
  on(ApplicationActions.logIn, (state, player) => {
    return {...state, player: player}
  }),
  on(ApplicationActions.logOut, (state) => {
    return {...state, player: undefined}
  }),
)

// Sync ngrx store with LocalStorage. See /doc/Storage.md
const reducerKeys = ['appState', 'cardGame'];
export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({keys: reducerKeys})(reducer);
}
export const metaReducers: MetaReducer<{appState: ApplicationState}>[] = [localStorageSyncReducer];
