import {Player} from "../shared/models/player.model";
import * as localStorageUtils from './localstorage.utils';

export interface ApplicationState {
  player: Player | undefined; // undefined if not logged in
}

export const initialAppState: ApplicationState = {
  player: localStorageUtils.getItem('appState').player,
}
