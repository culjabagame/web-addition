import {createSelector} from "@ngrx/store";

export const selectAppState = (state: any) => state.appState;

export const selectPlayer = createSelector(
  selectAppState,
  (state) => {return state.player}
);
