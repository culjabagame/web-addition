import {createActionGroup, emptyProps, props} from "@ngrx/store";
import {Player} from "../shared/models/player.model";

export const ApplicationActions = createActionGroup({
  source: 'APP',
  events: {
    'Log out': emptyProps(),
    'Log in': props<Player>(),
  }
});
