import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PlayersListComponent} from "./players/players-list/players-list.component";
import {CardGameComponent} from "./card-game/card-game/card-game.component";
import {StartScreenComponent} from "./card-game/start-screen/start-screen.component";
import {LoginComponent} from "./auth/login/login.component";
import {AuthGuardService} from "./auth/auth-guard.service";

const routes: Routes = [
  {path: '', redirectTo: 'players', pathMatch: 'full'},
  {path: 'auth/login', component: LoginComponent},
  {path: 'players', canActivate: [AuthGuardService], component: PlayersListComponent},
  {path: 'cards/start', canActivate: [AuthGuardService], component: StartScreenComponent},
  {path: 'cards', canActivate: [AuthGuardService], component: CardGameComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
