export class Player {
  id: number;
  name: string;
  worldX: number;
  worldY: number;
  online: boolean;
  createdAt: Date;
  updatedAt: Date;


  constructor(id: number, name: string, worldX: number, worldY: number, online: boolean, createdAt: Date, updatedAt: Date) {
    this.id = id;
    this.name = name;
    this.worldX = worldX;
    this.worldY = worldY;
    this.online = online;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}
