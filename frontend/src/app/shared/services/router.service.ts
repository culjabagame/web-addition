import { Injectable } from '@angular/core';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class RouterService {

  constructor(private router: Router) { }

  startCardGame(): Promise<boolean> {
    return this.router.navigate(['cards']);
  }

  navigateHome(): Promise<boolean> {
    return this.router.navigate(['players']);
  }

  navigateLogin() {
    return this.router.navigate(['auth/login']);
  }
}
